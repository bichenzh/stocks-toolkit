import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import requests
import os
from util import download, get_data, getRealTimeData
import indicators as id
import time

# symbols = ['JPM','MSFT','SPY','V','TSM','BILI','NIO','GOOG','AAPL','BABA','DIS','FB','TSLA','PDD','SPOT','ZM','KO','NFLX','LUV','RTX']
symbols = ['SPY']
today = dt.date.today()
ed = dt.datetime(today.year,today.month,today.day)
today_utc = ed.replace(tzinfo = dt.timezone.utc)
start =  dt.date.fromtimestamp(ed.timestamp()-126*60*60*24)
sd = dt.datetime(start.year, start.month, start.day)
dates = pd.date_range(sd, ed)

# download(symbol,sd,ed)
# time.sleep(0.1)

df = get_data(symbols, dates)
f_latest_ts = df.index[-1].timestamp()
today_utc_ts = pd.Timestamp(dt.date.today()).timestamp()
# print(f_latest_ts==today_utc_ts)

openPrice, dayHigh, dayLow, close, volume, adjclose = getRealTimeData(symbols[0])
# print(currentPrice, openPrice, dayLow, dayHigh, volume)
print(df.iloc[-2:])
df.loc[pd.Timestamp(dt.date.today()),symbols[0]] = adjclose 
print(df.iloc[-2:])