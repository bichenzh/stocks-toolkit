import os
import pandas as pd
import requests
import numpy as np
import datetime as dt
import re


def symbol_to_path(symbol, base_dir=None):
    """Return CSV file path given ticker symbol."""
    if base_dir is None:
        base_dir = os.environ.get("MARKET_DATA_DIR", './data/')
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def get_data(symbols, dates, addSPY=True, colname = 'Adj Close'):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', colname], na_values=['nan'])
        df_temp = df_temp.rename(columns={colname: symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])
    return df

def getHTMLContent(url):
    try:
        kv = {'user-agent':'Mozilla/5.0'}
        r = requests.get(url, timeout=30, headers=kv)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        return r.content
    except:
        return 'Cannot get the content'

def getHTMLText(url):
    try:
        kv = {'user-agent':'Mozilla/5.0'}
        r = requests.get(url, timeout=30, headers=kv)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        return r.text
    except:
        return 'Cannot get the text'

def download(symbol, sd, ed):
    path = r'./data/'+symbol+'.csv'
    sd_utc = sd.replace(tzinfo = dt.timezone.utc) 
    ed_utc = ed.replace(tzinfo = dt.timezone.utc) 
    sd_stamp = str(int(sd_utc.timestamp()))
    ed_stamp = str(int(ed_utc.timestamp())+60*60*24)
    url = 'https://query1.finance.yahoo.com/v7/finance/download/'+symbol+'?period1='+sd_stamp+'&period2='+ed_stamp+'&interval=1d&events=history'
    with open(path, 'wb+') as f:
        try:
            f.write(getHTMLContent(url))
        except:
            return 'Wrong download url, check the ticker symbol and timestamp'
    url = 'https://query1.finance.yahoo.com/v7/finance/download/SPY?period1='+sd_stamp+'&period2='+ed_stamp+'&interval=1d&events=history'
    with open(r'./data/SPY.csv', 'wb+') as f:
        try:
            f.write(getHTMLContent(url))
        except:
            return 'Wrong download url, check the ticker symbol and timestamp'
    pass

# def parseValue(text):
    # ls = re.findall(r'"'+target+'":{"raw":[-+]?[0-9]*\.?[0-9]+',text)
    # assert(len(ls)==1)
    # value = float(re.findall(r'[-+]?[0-9]*\.?[0-9]+',ls[0])[0])

    # return value


def getRealTimeData(symbol):
    url = 'https://finance.yahoo.com/quote/'+symbol+'/history?p='+symbol
    text = getHTMLText(url)

    currentInfo = re.search(r'[{"date":[0-9]*,"open":[-+]?[0-9]*\.?[0-9]+,"high":[-+]?[0-9]*\.?[0-9]+,"low":[-+]?[0-9]*\.?[0-9]+,"close":[-+]?[0-9]*\.?[0-9]+,"volume":[-+]?[0-9]*\.?[0-9]+,"adjclose":[-+]?[0-9]*\.?[0-9]+},'
        ,text).group()

    ls = re.findall(r'[-+]?[0-9]*\.?[0-9]+', currentInfo)

    openPrice = float(ls[1])
    dayHigh = float(ls[2])
    dayLow = float(ls[3])
    close = float(ls[4])
    volume = float(ls[5])
    adjclose = float(ls[6])

    return openPrice, dayHigh, dayLow, close, volume, adjclose
