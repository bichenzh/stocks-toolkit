import numpy as np
import pandas as pd
import datetime as dt
from util import get_data, getRealTimeData
import matplotlib.pyplot as plt


class indicators(object):
    """docstring for indicators"""
    def __init__(self, symbol, start_date, end_date, addCurrent = True):
        self.symbol = symbol
        self.dates = pd.date_range(start_date, end_date)
        # import adj close data which is a n*1 pd.df
        openPrice, dayHigh, dayLow, close, volume, adjclose = getRealTimeData(symbol)
        self.adjclose = get_data([symbol], self.dates, colname = 'Adj Close')
        self.adjclose = self.adjclose.iloc[:,-1:]
        self.adjclose.fillna(method = 'ffill', inplace = True)
        self.adjclose.fillna(method = 'bfill', inplace = True)
        if addCurrent:
            self.adjclose.loc[pd.Timestamp(dt.date.today()),symbol] = adjclose
        # import close data which is a n*1 pd.df
        self.close = get_data([symbol], self.dates, colname = 'Close')
        self.close = self.close.iloc[:,-1:]
        self.close.fillna(method = 'ffill', inplace = True)
        self.close.fillna(method = 'bfill', inplace = True)
        if addCurrent:
            self.close.loc[pd.Timestamp(dt.date.today()),symbol] = close
        # calculate the ratio between raw price with adjusted price
        self.ratio = self.adjclose/self.close
        # import high data which is a n*1 pd.df
        self.high = get_data([symbol], self.dates, colname = 'High')
        self.high = self.high.iloc[:,-1:]
        self.high.fillna(method = 'ffill', inplace = True)
        self.high.fillna(method = 'bfill', inplace = True)
        self.adjhigh = self.high*self.ratio
        if addCurrent:
            self.high.loc[pd.Timestamp(dt.date.today()),symbol] = dayHigh
            self.adjhigh.loc[pd.Timestamp(dt.date.today()),symbol] = dayHigh
        # import low data which is a n*1 pd.df
        self.low = get_data([symbol], self.dates, colname = 'Low')
        self.low = self.low.iloc[:,-1:]
        self.low.fillna(method = 'ffill', inplace = True)
        self.low.fillna(method = 'bfill', inplace = True)
        self.adjlow = self.low*self.ratio
        if addCurrent:
            self.low.loc[pd.Timestamp(dt.date.today()),symbol] = dayLow
            self.adjlow.loc[pd.Timestamp(dt.date.today()),symbol] = dayLow
        # import volume data which is a n*1 pd.df
        self.volume = get_data([symbol], self.dates, colname = 'Volume')
        self.volume = self.volume.iloc[:,-1:]
        self.volume.fillna(method = 'ffill', inplace = True)
        self.volume.fillna(method = 'bfill', inplace = True)
        if addCurrent:
            self.volume.loc[pd.Timestamp(dt.date.today()),symbol] = volume
        # import open data which is a n*1 pd.df
        # self.open = get_data([symbol], self.dates, colname = 'Open')
        # self.open = self.open.iloc[:,-1:]
        # self.open.fillna(method = 'ffill', inplace = True)
        # self.open.fillna(method = 'bfill', inplace = True)


    # Momentum (MTM) and rate of change (ROC) are simple 
    # technical analysis indicators showing the difference 
    # between today's closing price and the close N days ago.
    def momentum(self, period):
        period = int(period)
        if period >= self.adjclose.shape[0]:
            print("invalid period: period is too large")
        elif period < 1:
            print("invalid period: it should be greater than 0")
        else:
            momentum = self.adjclose - self.adjclose.shift(period)
            return momentum
    def momentum_roc(self, period):
        return self.momentum(period)/self.adjclose.shift(period)

    # Simple moving average
    # Relative difference with closed prices
    def sma(self, period):
        period = int(period)
        if period >= self.adjclose.shape[0]:
            print("invalid period: period is too large")
        elif period < 1:
            print("invalid period: it should be greater than 0")
        else:
            sma = self.adjclose.rolling(period).mean()
            return sma
    def sma_relative(self, period):
        return self.adjclose/self.sma(period) - 1

    # Bollinger Bands
    def bbands(self, period):
        return (self.adjclose - self.sma(period))/(2*self.adjclose.rolling(period).std())
    def bbands_boundaries(self, period):
        upper = self.sma(period) + 2*self.adjclose.rolling(period).std()
        lower = self.sma(period) - 2*self.adjclose.rolling(period).std()
        boundaries = pd.concat([upper,lower],axis=1)
        return boundaries
        
    # The money flow index (MFI) is an oscillator that ranges from 0 to 100.
    # It is used to show the money flow (an approximation of the dollar 
    # value of a day's trading) over several days.
    # Here, I have a daily MFI and make it ranges at (-1,1)
    def mfi(self, period):
        # typical price
        tprice = pd.DataFrame(pd.concat([self.high, self.low, self.close],axis=1).mean(axis=1),\
                                columns=[self.symbol])
        moneyflow = tprice * self.volume
        adjtprice = tprice = pd.DataFrame(pd.concat([self.adjhigh, self.adjlow, self.adjclose],axis=1).mean(axis=1),\
                                columns=[self.symbol])
        posmfdates = (adjtprice > adjtprice.shift(1)).values.reshape(tprice.shape[0])
        negmfdates = (adjtprice < adjtprice.shift(1)).values.reshape(tprice.shape[0])
        
        posmf = moneyflow.copy()
        negmf = moneyflow.copy()

        posmf.iloc[~posmfdates] = np.nan
        posmf.fillna(0,inplace = True)
        negmf.iloc[~negmfdates] = np.nan
        negmf.fillna(0,inplace = True)
        moneyratio = posmf.rolling(period).sum()/negmf.rolling(period).sum()
        mfi = 100.0 - 100.0/(1+moneyratio)
        return mfi

    # Relative Strength Index (RSI)
    def rsi(self, period):
        # Relative Strength
        change = self.adjclose-self.adjclose.shift(1)
        change.fillna(0)
        gain = change.applymap(lambda x: x if x>0 else 0)
        loss = change.applymap(lambda x: -x if x<0 else 0)
        averageGain = gain.rolling(period).mean()
        averageLoss = loss.rolling(period).mean()
        for x in xrange(period,gain.shape[0]):
            averageGain.iloc[x,0] = (averageGain.iloc[x-1,0]*(period-1) + gain.iloc[x,0])/period
            averageLoss.iloc[x,0] = (averageLoss.iloc[x-1,0]*(period-1) + loss.iloc[x,0])/period
        
        rs = averageGain/averageLoss
        # RSI
        return 100.0 - 100.0/(1+rs)



    
 